---Question 1---

#include <stdio.h>

void pattern(int Number);
void pattern(int Number){

    if (Number > 0) 
    {
            pattern(Number-1);
            while(Number >0)
	    {
                printf("%d",Number);
                Number = (Number -1);
            }
            printf("\n");

    }

}

int main()
{
    int Num;

    printf("Enter Number: ");
    scanf("%d", &Num); //Getting the keyboard inputs

    pattern(Num);

    return 0;
}